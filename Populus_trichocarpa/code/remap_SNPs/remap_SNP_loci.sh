#!/bin/bash
#SBATCH --job-name=Ptrich_v2.2_v4.1_remapping
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 6
#SBATCH --mem=50G
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=meghan.myles@uconn.edu
#SBATCH --output Ptrich_v2.2_v4.1_remapping-%j.out
#SBATCH --error Ptrich_v2.2_v4.1_remapping-%j.err

#!/bin/bash

#################################################################
# User input

INPUT_CSV="SNP_reference_genome_position_data.csv"
NEW_GENOME="/core/labs/Wegrzyn/PopTrich_2024/PopTrich_2024/genomes/v4.1/GCA_000002775.4_P.trichocarpa_v4.1_genomic.fna"
OUTPUT_CSV="SNP_loci_remapped.csv"

#################################################################

module load bwa

# Index the reference genome
bwa index "$NEW_GENOME"

# Create a temporary file to store flanks in FASTA format
awk -F',' '{print ">"$1"\n"$6"\n"$7}' "$INPUT_CSV" > flanks.fasta

# Align flanks to the new genome using BWA
bwa mem "$NEW_GENOME" flanks.fasta > aligned.sam

# Process alignment results to extract aligned locations and sequences
awk 'BEGIN {print "SNP_name,chromosome,snp_base_position_new_genome"; OFS=","}
     NR==FNR {a[$1]=$1; next}
     $1 !~ /^@/ {print a[$1], $3, $4}' aligned.sam > aligned_info.csv

# Output the new CSV file
awk 'BEGIN {FS=OFS=","}
     NR==FNR {a[$1]=$0; next}
     {print $1, $2, $3}' aligned_info.txt "$INPUT_CSV" > "$OUTPUT_CSV"
