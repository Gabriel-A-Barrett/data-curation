<h2> Species Images Curation


Our new protocol for uploading species images: When you are uploading your tppsc study and get to the final review page, if the page gives you an option to upload a species image, then follow the protocol below to search for it, update the spreadsheet with relevant licensing information, and upload the species image on that exact page!

[Species Image Curation SpreadSheet](https://docs.google.com/spreadsheets/d/1iBeDfVUq4PiVNBSDdNGKjxHSbXVlVPULn2sHEqFom0o/edit?folder=1Gz_BDhhsnNuRbnkQgvo5KgSac1gastcN#gid=2031590790): This links to the full list of species with no images in the database.

When uploading a new TPPSc study with a new species that does not live in our database, use the image field on the review page of the study to upload it, and then add the corresponding Attribution and License to the spreadsheet listed above. Ensure again that the species image name when uploaded is genus_species.jpg

FOR MASS SPECIES UPLOAD:

1.  Finding the Image
* Search [Google Images](https://images.google.com/) for the image. When using google, Goto tools and set Usage Rights to Labeled for noncommerical reuse. Most results will be of pages from flickr or wikimedia commons.
* If a google search did not display an image of said species, goto [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) and search for it there.
* If a wikimedia commons search still could not find a species image, check [Flickr](https://www.flickr.com/)
* If a search of Google, wikimedia commons, and flickr could not find a species image, state results and proceed to next species

2.  Once the Image is Found:
* Download the image to a localized folder. Name image genus_species.jpg
* Copy the URL of the image source into the Curation Spreadsheet
* Copy the Attribution and License into the Curation Spreadsheet

3. Uploading images to the Database:
* Images live at sites/default/files/treepictures. Uploaded all images from localized folder to this location
* Both the images and the copyright information need to be uploaded to the database
* After images are uploaded, remove species from Current Species Image List Sheet and Add species to Added to DB Sheet in the Curation Spreadsheet
* Ensure when doing mass species image upload, to download all the images to a localized folder.
